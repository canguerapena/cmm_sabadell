
name := "ValidationPO"

version := "0.0.1"

scalaVersion := "2.10.6"

mainClass in Compile := Some("com.dcm.validationpo.MainApp")

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.3"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.3"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.6.3"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.9"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api-scala_2.10" % "11.0"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.8.1"
// https://mvnrepository.com/artifact/org.apache.kafka/kafka
libraryDependencies += "org.apache.kafka" %% "kafka" % "0.9.0.0"


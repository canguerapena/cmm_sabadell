package com.dcm.validationpo
//import org.apache.spark.sql.SparkSession

import com.dcm.utils.{FileUtils,JsonUtils,KafkaMgrProducer,ReadTopUtils}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.{SparkConf, SparkContext}
import play.api.libs.json.{JsValue, Json}
//import org.apache.spark.sql.SparkSession
import scala.util.{Failure, Success}

object MainApp {

  val CONFIG_FILE = "./resources/configprocess.json"
  val CONFIG_FORMAT = List("localPoDir,String",
    "numPoFiles,Int",
    "regExprHeader,String",
    "regExprDetail,String",
    "remotePoRawHdfs,String",
    "topicRaw,String",
    "localPoDirCon,String",
    "numPoFilesCon,Int",
    "regExprHeaderCon,String",
    "regExprDetailCon,String",
    "remotePoConHdfs,String",
    "topicCl,String",
    "hdfsMaster,String",
    "zkpMaster,String",
    "brokers,String")

  def main(args : Array[String]) : Unit = {

    val fUtils = new FileUtils()
    val jsonUtils = new JsonUtils()
    //Character to be removed from json Strings
    val toRemove  = "\"".toSet

    //Cargar configuracion Json
    println("Loading configuration")

    val conf = new SparkConf()
      .setAppName("DBGenerator")
      .setMaster("local")
    val sc = new SparkContext(conf)

    //Read json
    val fileData = fUtils.readTextFile(CONFIG_FILE)

    val jsonConfigStr = fileData match {
      case Success(lines) => lines.mkString("")
      case Failure(f) =>
        println(f)
        sys.exit(1)
    }
    val jsonData: JsValue = Json.parse(jsonConfigStr)
    println(jsonData)
/*
    println("********************************************")
    println("Create notification to send to topic RAW")
    println("********************************************")
    val messageRaw = jsonUtils.createEventMessage(
      poFiles(0),
      jsonData("localPoDir").toString().filterNot(toRemove),
      poFiles(1),
      jsonData("localPoDir").toString().filterNot(toRemove))
    println(messageRaw)
    /* Send the message to the topic */
    val topicRa = jsonData("topicRaw").toString().filterNot(toRemove)
    val brokers = jsonData("brokers").toString().filterNot(toRemove)
    val kMgr = new KafkaManager(topicRa, brokers, "MonitorPO")
    //TODO: Check the kafka imports that are failing in Intellij
    kMgr.sendMessage(messageRaw)
    */

    println("********************************************")
    println("Run new messages in Topic raw_po")
    println("********************************************")


    val topic = jsonData("topicRaw").toString().filterNot(toRemove)
    val zkp = jsonData ("zkpMaster").toString().filterNot(toRemove)
    val ClientId = "ValidationPO"
    val consumer = new ReadTopUtils(topic, ClientId, zkp)

    val confit = new SparkConf()
      .setAppName("ProcessPO")
      //El local se tendra que cambiar cuando se pruebe en el cluster
      .setMaster("local")
    val scp = new SparkContext(confit)
    val sqlContext = new org.apache.spark.sql.SQLContext(scp)


    while (true) {
      consumer.read() match {
        case Some(messagesRaw) =>
          println("Getting message.......................  " + messagesRaw)
        // wait for 100 milli second for another read
        //Thread.sleep(100)

          //Realizo acciones si encuentra mensaje nuevo
          //Primero es leer el mensaje , que es parsenado el json que me viene de la clase JsonUtils del metodo createEvent Messa
          //en este caso el el mensaje que me viene como messageRaw
          val jsonMsg: JsValue = Json.parse(messagesRaw)
          //Leo el cmapo fileName del header y lo mismo con los otros
          val fileNameHeader= (jsonMsg \ "header" \ "fileName").get
          val fileNameDetail= (jsonMsg \ "detail" \ "fileName").get
          //Leo el campo ruta hdfs para cada ficher
          val pathHeaderHdfs= (jsonMsg \ "header" \ "filePath").get
          val pathDetailHdfs= (jsonMsg \ "detail" \ "filePath").get

          //Leer los PO de HDFS como DataFrame



          //Lectura del csv como DF del Cabecera
          val dfCabeceraBacRaw = sqlContext.read.format("com.databricks.spark.csv")
            //Opcion con cabec , nombre de las column
            .option("header", "true")
            .option("inferSchema", "true")
            .load(pathHeaderHdfs.toString()+ "\\" + fileNameHeader.toString()).toDF()
            //.load("file:///C:\\ProyectoBS\\Desarrollo\\cmm_sabadell\\pogenerate\\src\\test\\resources\\results\\cabecera.csv").toDF()


          //Lectura del csv como DataFrame del Detal
          val dfDetailBacRaw = sqlContext.read.format("com.databricks.spark.csv")
            //Opcion con cabec , nombre de las column
            .option("header", "true")
            .option("inferSchema", "true")
            .load(pathDetailHdfs.toString()+ "\\" + fileNameDetail.toString()).toDF()


        case None =>
          println("Queue is empty.......................  ")
          // wait for 2 second
          Thread.sleep(2 * 1000)

      }
    }



    println("********************************************")
    println("Looking for P.O. files Consolidate after the Process")
    println("********************************************")
    /* Check P.O. path to look for Consolidate files*/
    //TODO: Detect if the poFiles found have been processed as Ok
    val poFilesCo = fUtils.findNewPoFiles(jsonData)

    /* If the P.O. files Consolidate have been found then save them into hdfs,
     * create the notification message
     * and send it to the "clean_po" kafka topic
    */
    if (poFilesCo.length != 0){
      println("********************************************")
      println("Save clean P.O.s in HDFS")
      println("********************************************")

      /* Save clean P.O. files Consolidat in HDFS */
      //TODO: Run this code in our cluster
      try{
        val conf = new Configuration()
        conf.set("fs.defaultFS", jsonData("hdfsMaster").toString().filterNot(toRemove))
        val hdfs= FileSystem.get(conf)
        for (poFile <- poFilesCo) {
          try {
            val srcPath = new Path(jsonData("localPoDirCon").toString().filterNot(toRemove) + "/" + poFile)
            val destPath = new Path(jsonData("remotePoConHdfs").toString().filterNot(toRemove))
            hdfs.copyFromLocalFile(srcPath, destPath)
          }
        }
      }
      catch{
        case e: Exception =>
          println(e)
      }

      println("********************************************")
      println("Create notification to send to topic clean")
      println("********************************************")
      val messageClean = jsonUtils.createEventMessage(
        poFilesCo(0),
        jsonData("remotePoConHdfs").toString().filterNot(toRemove),
        poFilesCo(1),
        jsonData("remotePoConHdfs").toString().filterNot(toRemove))
      println(messageClean)
      /* Send the message to the topic Clean */
      val topic = jsonData("topicCl").toString().filterNot(toRemove)
      val brokers = jsonData("brokers").toString().filterNot(toRemove)
      val kMgr = new KafkaMgrProducer(topic, brokers, "MonitorPO")
      //TODO: Check the kafka imports that are failing in Intellij
      kMgr.sendMessage(messageClean)
    }

    sc.stop()
    println("*****************END PO BEFORE BAC*****************")

    //TODO: Create unit tests
  }
}

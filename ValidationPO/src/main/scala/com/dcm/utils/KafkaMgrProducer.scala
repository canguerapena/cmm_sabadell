package com.dcm.utils

import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

//KAFKA PRODUCTOR

class KafkaMgrProducer(topic: String, brokers : String, clientId: String) {

  val props = new Properties()
      props.put("bootstrap.servers", brokers)
      props.put("client.id", clientId)
      props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
      props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  def sendMessage(message: String): Unit =
  {
    val producer = new KafkaProducer[String, String](props)
    val data = new ProducerRecord[String, String](topic, "", message)
    producer.send(data)
    producer.close()
  }


}

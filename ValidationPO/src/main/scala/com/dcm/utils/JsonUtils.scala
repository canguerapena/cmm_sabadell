package com.dcm.utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}
import play.api.libs.json.{JsValue, Json}

class JsonUtils {

  /**
    * Validate the content of the configuration file
    *
    * @param jsonData
    * @param fieldAndTypes
    * @return true if all is right and false otherwise
    */
  def validateConfigJson(jsonData: JsValue, fieldAndTypes : List[String]) : Boolean = {
    var res = true
    if (jsonData.equals(None) == false){
      for(fieldAndType <- fieldAndTypes
          if res == true){
        val fieldType = fieldAndType.split(",")
        try {
          val fType = fieldType(1) match {
            case "String" =>
              (jsonData \ fieldType(0)).as[String]
              res = true
            case "Int" =>
              (jsonData \ fieldType(0)).as[Int]
              res = true
            case "List[String]" =>
              (jsonData \ fieldType(0)).as[List[String]]
              res = true
            case _ => res = false
          }
        }
        catch {
          case e: Exception =>
            println("Error in configuration file: " + e)
            res = false
        }
      }
    }
    res
  }

  def createEventMessage(poHeaderFileName: String, poHeaderPath: String, poDetailFileName: String, poDetailPath: String) : String = {

    val fUtils = new FileUtils()
    val headerCreationDate = fUtils.getCreationDate(poHeaderFileName, poHeaderPath)
    val detailCreationDate = fUtils.getCreationDate(poDetailFileName, poDetailPath)
    val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val now = Calendar.getInstance()

    val readDate = formatter.format(Date.from(now.toInstant))


    val json: JsValue = Json.obj(
      "header" -> Json.obj("fileName" -> poHeaderFileName,
                                        "creationDate" -> headerCreationDate,
                                        "readDate" -> readDate,
                                        "filePath" -> poHeaderPath,
                                        "idLoad" -> now.toInstant.getEpochSecond),
            "detail" -> Json.obj("fileName" -> poDetailFileName,
                                        "creationDate" -> detailCreationDate,
                                        "readDate" -> readDate,
                                        "filePath" -> poDetailPath,
                                        "idLoad" -> now.toInstant.getEpochSecond)
    )

    json.toString()
  }
}

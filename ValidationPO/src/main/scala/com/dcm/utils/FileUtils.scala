package com.dcm.utils

import java.io.File
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{Files, Paths}
import java.text.SimpleDateFormat
import java.util.Date
import play.api.libs.json.JsValue
import scala.io.Source
import scala.util.Try

class FileUtils {

  /**
    * Read the content of a file
    *
    * @param filename
    * @return a list of strings where each string is a line of the text
    *         if everything is ok or the message of the exception if something
    *         is wrong
    */
  def readTextFile(filename: String): Try[List[String]] = {
    Try(Source.fromFile(filename).getLines.toList)
  }

  /**
    * Get the list of file names in the given directory
    *
    * @param dirName
    * @return a list with the file names in the given directory
    */
  def getListOfFiles(dirName: String): List[String] = {
    val d = new File(dirName)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).map(_.getName).toList
    } else {
      println("NO EXISTE")
      List[String]()
    }
  }

  /**
    * Get the list of subdirectory names in the given directory
    *
    * @param dirName
    * @return a list with the name of the subdirectories
    */
  def getListOfSubDirectories(dirName: String): List[String] = {
    val d = new File(dirName)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isDirectory).map(_.getName).toList
    } else {
      List[String]()
    }
  }

  /**
    * Find the P.O. files Consolidate that match the configuration given in the json
    *
    * @param jsonData
    * @return a list with the P.O. files Conso or an empty list if the number of found
    *         files is wrong or the names don't match the regular expression
    */
  def findNewPoFiles(jsonData: JsValue) : List[String] = {

    /* Remove \" from the string */
    val toRemove  = "\"".toSet
    //path where are the PO Consolidate if the process finisk Ok
    val dirName   = jsonData("localPoDirCon").toString().filterNot(toRemove)
    val fileNames = getListOfFiles(dirName)

    /* Get the list of regular expressions to apply to each file */
    /* Notice that the order of the regular expressions is important:
     *  First one is the header
     *  Second one is detail
    */
    val regExprHeader    = jsonData("regExprHeaderCon").toString().filterNot(toRemove)
    val regExprDetail    = jsonData("regExprDetailCon").toString().filterNot(toRemove)
    val regExpr = List(regExprHeader, regExprDetail)

    val numPoFilesCo = (jsonData \ "numPoFilesCon").as[Int]
    val poFilesCo    = new Array[String](numPoFilesCo)

    /* Check the pattern */
    if (fileNames.length == numPoFilesCo) {
      for (fileName <- fileNames) {
        for ((expr, i) <- (regExpr zip List.range(0,numPoFilesCo))) {
          val pattern = expr.r
          if ((pattern findFirstIn fileName) != None) {
            poFilesCo(i) = fileName
          }
        }
      }
      if (poFilesCo.length != numPoFilesCo)
        List[String]()
    }

    poFilesCo.toList
  }

  /**
    * Get the creation date of the given file
    *
    * @param fileName
    * @param filePath
    * @return
    */
  def getCreationDate(fileName : String, filePath : String, datePattern : String = "yyyy-MM-dd HH:mm:ss") : String = {

    var creationDate = ""
    val absPath = filePath + "/" + fileName
    val file = new File(absPath)
    if (file.exists && file.isFile) {
      val formatter = new SimpleDateFormat(datePattern)
      val instantCreationDate = Files.readAttributes(Paths.get(absPath), classOf[BasicFileAttributes]).creationTime.toInstant()
      creationDate = formatter.format(Date.from(instantCreationDate))
    }
    creationDate
  }

}

package com.bs.dcm.bac.utils

object BacConstants {

  val database = "dcm"
  val table_po_procesado = database+"."+"po_procesado"
  val table_dcm_cabecera_bac = database+"."+"dcm_cabecera_bac"
  val table_dcm_cabecera_bac_hist = database+"."+"dcm_cabecera_bac_hist"
  val table_dcm_detalle_bac = database+"."+"dcm_detalle_bac"
  val table_dcm_detalle_bac_hist = database+"."+"dcm_detalle_bac_hist"

  val PO_PROC_COL_NAMES = Seq("fecha_creacion", "nombre_fichero", "fecha_procesado")

  //Character to be removed from json Strings
  val toRemove  = "\"".toSet

  val CONFIG_FORMAT_MONITORPO: List[(String, String)] = List(("localPoDir", "String"),
    ("numPoFiles", "Int"),
    ("regExprHeader", "String"),
    ("regExprDetail", "String"),
    ("remotePoDir", "String"),
    ("hdfsMaster", "String"),
    ("topic", "String"),
    ("brokers", "String"))

  val CONFIG_FORMAT_PROCESSPO : List[(String,String)] = List(("hdfsMaster","String"),
    ("topic","String"),
    ("brokers","String"))

  val CONFIG_FILE_VALIDATIONPO = "./resources/configprocess.json"
  val CONFIG_FORMAT_VALIDATIONPO : List[(String,String)] = List(("localPoDir","String"),
    ("numPoFiles","Int"),
    ("regExprHeader","String"),
    ("regExprDetail","String"),
    ("remotePoRawHdfs","String"),
    ("topicRaw","String"),
    ("localPoDirCon","String"),
    ("numPoFilesCon","Int"),
    ("regExprHeaderCon","String"),
    ("regExprDetailCon","String"),
    ("remotePoConHdfs","String"),
    ("topicCl","String"),
    ("hdfsMaster","String"),
    ("zkpMaster","String"),
    ("brokers","String"))

  /* List of column names to remove from PO because they aren't used in new BAC */
  val PO_COLS_TO_REMOVE_CABECERA = Seq("ID_CAMPANYA_SGC", "ID_ACCION_SGC", "ID_TARGET_SGC", "ID_VERSION_SGC", "SEQACCIO_INI",
    "TIPO_EJECUCION", "ID_ORIGEN", "IND_FILTROS_RAI", "IND_OTROS_TRAT", "TIPATR1", "TIPATR2", "TIPATR3", "TIPATR4",
    "TIPATR5", "TIPATR6", "TIPATR7", "TIPATR8", "IND_CARTERA_BLINDADA", "QS_LIST_SOPORTE", "QS_VIS_FICHA",
    "IND_ACCION_DEPURACION", "PROGRAMA", "ID_PRODUCTO_CAMPANYA", "IND_INTEGRACION_CRM", "IND_CAMPANYA_COMERCIAL",
    "USUARI", "INDBLQ", "COD_OTROS_TRAT", "ind_enviar_canal")
  /* List of columns names in new BAC */
  val PO_FINAL_COL_NAMES_CAB = Seq("UN", "COD_CAMP", "COD_ACCION", "COD_TARGET", "SEQ_ACCION", "ID_CANAL", "ID_MEDIO",
    "ID_PROD", "ID_NIVEL_AUD", "ID_OBJ_CLIENTE", "ID_OBJ_PROD", "ESTADO_ACCION", "FORATR1", "FORATR2", "FORATR3",
    "FORATR4", "FORATR5", "FORATR6", "FORATR7", "FORATR8", "FECHA_INI_ACC", "FECHA_FIN_ACC", "DESTINO_ACC",
    "TIPO_ACCION", "MODALIDAD_INT", "TRATAMIENTO", "FECHA_DISTRIB", "TOTAL_REGISTROS", "TIMESTAMP_BAC", "USU_CA_ES",
    "TIMESTAMP_CA_ES", "TIMESTAMP_DIST", "TIMESTAMP_5VA")
  val COL_NAMES_NOT_IN_PO_CAB = Map("ID_PRIORIDAD_ACC" -> "", "FECHA_INTEGRACION" -> "", "FECHA_ENV_CANAL" -> "", "CELL_PACKAGE_SK" -> 0)
  //val COL_NAMES_NOT_IN_PO_CAB = Seq("ID_PRIORIDAD_ACC", "FECHA_INTEGRACION", "FECHA_ENV_CANAL", "CELL_PACKAGE_SK")
  val FIELD_KEY_NAMES_CAB = Seq("UN", "COD_CAMP", "COD_ACCION", "COD_TARGET", "SEQ_ACCION")

  val PO_COLS_TO_REMOVE_DETALLE = Seq("SUBSEQACCIO", "ID_UNIDAD_OBJETIVO", "CODENTGEST", "INGCON", "INCLIE", "VSGCFN",
    "NOMTOT", "PRICOG", "SEGCOG", "SEXPER", "CODERR", "PRE_INT_BSMOVIL", "INDBLQ", "ID_FCH_RECH_CANAL", "USUARECH_CANAL",
    "NIF_DIRECTOR", "NOMBRE_DIRECTOR", "BIDI")
  val PO_FINAL_COL_NAMES_DET = Seq("UN", "COD_CAMP", "COD_ACCION", "SEQ_ACCION", "COD_TARGET", "NUMPER", "ID_DETALLE", "IDIOMA",
    "CENTRO_GESTOR", "CARTERA", "OFICINA_GESTORA", "CLAVE_ASIG", "NIF", "GC_DISTRIB", "MARCA", "ATRIBUT1", "ATRIBUT2", "ATRIBUT3",
    "ATRIBUT4", "ATRIBUT5", "ATRIBUT6", "ATRIBUT7", "ATRIBUT8", "NOMBRE", "TIPO_PERSONA", "ENT_BUZON", "OFI_BUZON",
    "DOMICILIO", "COMP_DOM", "POBLACION", "COD_PAIS", "CLAVE_ENVIO", "IND_CORRESP", "TIPO_REP", "COD_DEVOLUCION",
    "TITULO1", "TITULO2", "NUMPERSIG", "TELEFONO1", "TELEFONO2", "TELEFONO_BSMOVIL", "DIREMAIL", "TSTAMP", "USUARIO",
    "CODIRECH", "USUARECH", "TSTAMPRECH", "ID_ESTADO_ACCPERS", "ID_RESULTADO_ACCPERS", "ID_MOTIVO_RECH_CANAL",
    "ATR_CANAL_A1", "ATR_CANAL_A2", "ATR_CANAL_A3", "ATR_CANAL_A4", "ATR_CANAL_A5", "ATR_CANAL_A6", "ATR_CANAL_A7",
    "ATR_CANAL_A8", "ATR_CANAL_A9", "ATR_CANAL_A10", "ATR_CANAL_N1", "ATR_CANAL_N2", "ATR_CANAL_N3", "ATR_CANAL_N4",
    "ATR_CANAL_N5", "ATR_CANAL_N6", "ATR_CANAL_N7", "ATR_CANAL_N8", "ATR_CANAL_N9", "ATR_CANAL_N10", "ATR_CANAL_F1",
    "ATR_CANAL_F2", "ATR_CANAL_F3", "ATR_CANAL_F4", "ATR_CANAL_F5")
  val COL_NAMES_NOT_IN_PO_DET = Map("TIPUS_CARTERA" -> "", "FUNCION" -> "", "EMAIL_GESTOR" -> "", "NOMBRE_GESTOR" -> "",
    "APELLIDO1_GESTOR" -> "", "APELLIDO2_GESTOR" -> "", "NOMBRE_OFICINA" -> "", "DIRECCION_OFICINA" -> "",
    "CODIGO_POSTAL_OFICINA" -> "", "POBLACION_OFICINA" -> "", "TELEFONO_OFICINA" -> "", "MAIL_OFICINA" -> "",
    "RUD_C_GES_C_FINAL" -> 0, "SW_GRUPO_CONTROL" -> "", "SW_SEMILLA" -> "", "APELLIDO1" -> "", "APELLIDO2" -> "",
    "ID_SEXO" -> "", "EDAD" -> 0, "FCHA_NACIMIENTO" -> "", "FCHA_RESULTADO" -> "", "FCHA_INTEGRACION" -> "",
    "FCHA_RECH_CANAL" -> "", "FCHA_ENV_CANAL" -> "", "ID_CONTACTO" -> 0, "ID_CONTRATO" -> 0, "ID_POTENCIAL" -> 0)
  val FIELD_KEY_NAMES_DET = Seq("UN", "COD_CAMP", "COD_ACCION", "COD_TARGET", "SEQ_ACCION")


}

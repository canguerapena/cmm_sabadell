package com.bs.dcm.bac.validation


import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}
object validationPOMain {

  def main(args : Array[String]) : Unit = {

    val conf = new SparkConf()
      .setAppName(this.getClass.toString)
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)
    hiveContext.sql("SET hive.exec.dynamic.partition = true")
    hiveContext.sql("SET hive.exec.dynamic.partition.mode = nonstrict ")

    var confFile = ""
    if (args.isEmpty) {
      println("Error, no configuration file given")
      sys.exit(1)
    }
    confFile = args(0)

    val validation_po = new validationPO()
    validation_po.execute(sc,hiveContext,confFile)
    sc.stop()
  }
}

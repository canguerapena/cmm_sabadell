package com.bs.dcm.bac.utils

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}
import com.bs.dcm.bac.utils.FileUtils._
import scala.util.parsing.json.JSONObject

object JsonUtils {

  def getFromJson( json : Any, key : String) : Any ={
    json match {
      case (m: Map[String, Any]) => m(key)
      case Some(m: Map[String, Any]) => m(key)
    }
  }

  def getFromJson( json : Map[String,Any], key : String) : Any ={
    json(key)
  }

  /**
    * Validate the content of the configuration file
    *
    * @param jsonData
    * @param fieldAndTypes
    * @return true if all is right and false otherwise
    */
  def validateConfigJson(jsonData: Option[Any], fieldAndTypes : List[(String,String)]) : Boolean = {
    var res = true
    if (jsonData.equals(None) == false){
      for(fieldAndType <- fieldAndTypes
          if res == true){
        val fieldType = fieldAndType._2
        try {
          val fType = fieldType match {
            case "String" =>
              getFromJson(jsonData,fieldAndType._1).isInstanceOf[String]
              res = true
            case "Int" =>
              getFromJson(jsonData,fieldAndType._1).isInstanceOf[Int]
              res = true
            case "List[String]" =>
              getFromJson(jsonData,fieldAndType._1).isInstanceOf[Array[String]]
              res = true
            case _ => res = false
          }
        }
        catch {
          case e: Exception =>
            println("Error in configuration file: " + e.printStackTrace())
            res = false
        }
      }
    }
    res
  }

  def createEventMessage(poHeaderFileName: String, poLocalHeaderPath: String, poRemoteHeaderPath: String,
                         poDetailFileName: String, poLocalDetailPath: String, poRemoteDetailPath: String) : JSONObject = {


    val headerCreationDate = getCreationDate(poHeaderFileName, poLocalHeaderPath)
    val detailCreationDate = getCreationDate(poDetailFileName, poLocalHeaderPath)
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val now = Calendar.getInstance()

    val readDate = formatter.format(Date.from(now.toInstant))

    val json = JSONObject.apply(Map("header" -> JSONObject.apply(Map("name" -> poHeaderFileName,
      "creationDate" -> headerCreationDate,
      "readDate" -> readDate,
      "filePath" -> poRemoteHeaderPath,
      "idLoad" -> now.toInstant.getEpochSecond)
    ),
      "detail" -> JSONObject.apply(Map("name" -> poDetailFileName,
        "creationDate" -> detailCreationDate,
        "readDate" -> readDate,
        "filePath" -> poRemoteDetailPath,
        "idLoad" -> now.toInstant.getEpochSecond))))


    json
  }
}

name := "ValidationPO"

version := "0.2"

scalaVersion := "2.10.5"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.3" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.3" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "1.6.3" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.6.3" % "provided"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api-scala_2.10" % "11.0" % "provided"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.8.1" % "provided"
libraryDependencies += "org.apache.kafka" %% "kafka" % "0.9.0.0" % "provided"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.9.0.0" % "provided"

//uses compile classpath for the run task, including "provided" jar (cf http://stackoverflow.com/a/21803413/3827)
//run in Compile := Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run)).evaluated

scalacOptions ++= Seq("-deprecation", "-unchecked")
pomIncludeRepository := { x => false }

resolvers ++= Seq(
  "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Second Typesafe repo" at "http://repo.typesafe.com/typesafe/maven-releases/",
  Resolver.sonatypeRepo("public")
)

pomIncludeRepository := { x => false }
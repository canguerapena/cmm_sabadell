package com.bs.dcm.bac.validation

import com.bs.dcm.bac.utils.{KafkaManager, ReadTopUtils}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.SparkContext

import scala.util.{Failure, Success}
import com.bs.dcm.bac.utils.BacConstants._
import com.bs.dcm.bac.utils.JsonUtils._
import com.bs.dcm.bac.utils.FileUtils._

import scala.util.parsing.json.JSON

class validationPO {

  def execute(sc: SparkContext, hiveContext: HiveContext, confFile: String): Unit = {

    //Cargar configuracion Json
    println("Loading configuration")

    //Read json
    val fileData = readTextFile(CONFIG_FILE_VALIDATIONPO)

    val jsonConfigStr = fileData match {
      case Success(lines) => lines.mkString("")
      case Failure(f) =>
        println(f)
        sys.exit(1)
    }
    val jsonData : Option[Any] =  JSON.parseFull(jsonConfigStr)
    println(jsonData)
/*
    println("********************************************")
    println("Create notification to send to topic RAW")
    println("********************************************")
    val messageRaw = jsonUtils.createEventMessage(
      poFiles(0),
      jsonData("localPoDir").toString().filterNot(toRemove),
      poFiles(1),
      jsonData("localPoDir").toString().filterNot(toRemove))
    println(messageRaw)
    /* Send the message to the topic */
    val topicRa = jsonData("topicRaw").toString().filterNot(toRemove)
    val brokers = jsonData("brokers").toString().filterNot(toRemove)
    val kMgr = new KafkaManager(topicRa, brokers, "MonitorPO")
    //TODO: Check the kafka imports that are failing in Intellij
    kMgr.sendMessage(messageRaw)
    */

    println("********************************************")
    println("Run new messages in Topic raw_po")
    println("********************************************")

    val topic = getFromJson(jsonData,"topicRaw").toString.filterNot(toRemove)
    val zkp = getFromJson(jsonData,"zkpMaster").toString.filterNot(toRemove)
    val ClientId = "ValidationPO"
    val consumer = new ReadTopUtils(topic, ClientId, zkp)

    while (true) {
      consumer.read() match {
        case Some(messagesRaw) =>
          println("Getting message.......................  " + messagesRaw)
        // wait for 100 milli second for another read
        //Thread.sleep(100)

          //Realizo acciones si encuentra mensaje nuevo
          //Primero es leer el mensaje , que es parsenado el json que me viene de la clase JsonUtils del metodo createEvent Messa
          //en este caso el el mensaje que me viene como messageRaw
          val jsonMsg : Option[Any] =  JSON.parseFull(messagesRaw)
          val header = getFromJson(jsonMsg,"header")
          val detail = getFromJson(jsonMsg,"detail")
          //Leo el cmapo fileName del header y lo mismo con los otros
          val fileNameHeader= getFromJson(header,"fileName").toString.filterNot(toRemove)
          val fileNameDetail= getFromJson(detail,"fileName").toString.filterNot(toRemove)
          //Leo el campo ruta hdfs para cada ficher
          val pathHeaderHdfs= getFromJson(header,"filePath").toString.filterNot(toRemove)
          val pathDetailHdfs= getFromJson(detail,"filePath").toString.filterNot(toRemove)

          //Leer los PO de HDFS como DataFrame



          //Lectura del csv como DF del Cabecera
          val dfCabeceraBacRaw = hiveContext.read.format("com.databricks.spark.csv")
            //Opcion con cabec , nombre de las column
            .option("header", "true")
            .option("inferSchema", "true")
            .load(pathHeaderHdfs.toString + "\\" + fileNameHeader.toString).toDF()
            //.load("file:///C:\\ProyectoBS\\Desarrollo\\cmm_sabadell\\pogenerate\\src\\test\\resources\\results\\cabecera.csv").toDF()


          //Lectura del csv como DataFrame del Detal
          val dfDetailBacRaw = hiveContext.read.format("com.databricks.spark.csv")
            //Opcion con cabec , nombre de las column
            .option("header", "true")
            .option("inferSchema", "true")
            .load(pathDetailHdfs.toString + "\\" + fileNameDetail.toString).toDF()


        case None =>
          println("Queue is empty.......................  ")
          // wait for 2 second
          Thread.sleep(2 * 1000)

      }
    }



    println("********************************************")
    println("Looking for P.O. files Consolidate after the Process")
    println("********************************************")
    /* Check P.O. path to look for Consolidate files*/
    //TODO: Detect if the poFiles found have been processed as Ok
    val poFilesCo = findNewPoFiles(jsonData)

    /* If the P.O. files Consolidate have been found then save them into hdfs,
     * create the notification message
     * and send it to the "clean_po" kafka topic
    */
    if (poFilesCo.nonEmpty){
      println("********************************************")
      println("Save clean P.O.s in HDFS")
      println("********************************************")

      /* Save clean P.O. files Consolidat in HDFS */
      //TODO: Run this code in our cluster
      try{
        val conf = new Configuration()
        val hdfsMaster = getFromJson(jsonData,"hdfsMaster").toString.filterNot(toRemove)
        val localPoDirCon = getFromJson(jsonData,"localPoDirCon").toString.filterNot(toRemove)
        val remotePoConHdfs = getFromJson(jsonData,"remotePoConHdfs").toString.filterNot(toRemove)
        conf.set("fs.defaultFS", hdfsMaster)
        val hdfs= FileSystem.get(conf)
        for (poFile <- poFilesCo) {
          try {
            val srcPath = new Path(localPoDirCon + "/" + poFile)
            val destPath = new Path(remotePoConHdfs)
            hdfs.copyFromLocalFile(srcPath, destPath)
          }
        }
      }
      catch{
        case e: Exception =>
          println(e)
      }

      println("********************************************")
      println("Create notification to send to topic clean")
      println("********************************************")
      val remotePoConHdfs = getFromJson(jsonData,"remotePoConHdfs").toString.filterNot(toRemove)
      val messageClean = createEventMessage(
        poFilesCo.head,
        "",
        remotePoConHdfs,
        poFilesCo(1),
        "",
        remotePoConHdfs)

      /* Send the message to the topic Clean */
      val topic = getFromJson(jsonData,"topicCl").toString.filterNot(toRemove)
      val brokers = getFromJson(jsonData,"brokers").toString.filterNot(toRemove)
      val kMgr = new KafkaManager(topic, brokers, "MonitorPO")
      //TODO: Check the kafka imports that are failing in Intellij
      kMgr.sendMessage(messageClean.toString())
    }

    sc.stop()
    println("*****************END PO BEFORE BAC*****************")

    //TODO: Create unit tests
  }
}

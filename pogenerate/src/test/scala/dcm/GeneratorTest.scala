package dcm

import org.scalatest.FunSuite

class GeneratorTest extends FunSuite {

  val json = new JsonParser("src/test/resources/detalleTest.json").read.parse
  val detGen = new Generator(json)

  test("Generar cabecera working") {
    detGen.generarCabecera()
    assert(detGen.getCabecera() == List("UN", "CAMPANYA", "fecha"))
  }

  test("Generar registro para el csv de la parte de detalle") {
    val registro = detGen.generarRegistro()
    // checkeamos el numero de campos que ha generado debe de ser tantos como definidos en el fichero de test detalleTest.json
    assert(registro.length == 3)
    // primer elemento es numerico
    assert(registro.head.length == 4)
    assert(registro.head forall Character.isDigit)

    // segundo elemento tamaño 4, no numerico
    assert(registro(1).length == 4)

    println(registro(2))
    assert(registro(2).length == 19)
  }

  test("Generar registros test") {
    detGen.generarRegistros()
    val registros = detGen.getRegistros()
    assert(registros.size == 1)
  }

  test("Get all unified structure") {
    detGen.generarCabecera()
    detGen.generarRegistros()
    val resultado = detGen.getResultado()
    assert(resultado.size == 2)
  }

}

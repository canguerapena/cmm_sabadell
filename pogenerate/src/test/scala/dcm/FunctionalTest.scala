package dcm

import org.scalatest.FunSuite

import scala.io.Source

class FunctionalTest extends FunSuite {

  test("Testing whole functionality") {
    val program = Main_po
    program.main(Array("src/test/resources/poconfig.json"))

    val fileCabecera = Source.fromFile("src/test/resources/results/cabecera.csv").getLines().toList
    val fileDetalle = Source.fromFile("src/test/resources/results/detalle.csv").getLines().toList

    assert(fileCabecera.size == 501)
    assert(fileDetalle.size == 501)

    assert(fileCabecera.head.split(",").size == 58)
    assert(fileDetalle.head.split(",").size == 92)
  }
}

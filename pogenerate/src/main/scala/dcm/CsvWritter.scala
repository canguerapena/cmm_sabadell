package dcm

import java.io.PrintWriter

object CsvWriter {

  def write(path: String, body: List[List[String]], delimiter: String) =
    new PrintWriter(path) { body.foreach(x => write(x.mkString(delimiter) + "\n")); close }
}

package dcm


object Main_po {

  case class CsvPaths(detalleCsvPath: String, cabeceraCsvPath: String)

  private def getCsvPaths(configJson: Map[String, Object]): CsvPaths = CsvPaths(
    configJson.get("dcm_detalle_bac") match {
      case None => throw new Exception("Error obraining path to write detalle csv, maybe wrong parametrization")
      case Some(x: Map[String, Object]) => x.get("ruta") match {
        case None => throw new Exception("Error obraining path to write detalle csv, maybe wrong parametrization")
        case Some(x: String) => x
      }
    },
    configJson.get("dcm_cabecera_bac") match {
      case None => throw new Exception("Error obraining path to write detalle csv, maybe wrong parametrization")
      case Some(x: Map[String, Object]) => x.get("ruta") match {
        case None => throw new Exception("Error obraining path to write detalle csv, maybe wrong parametrization")
        case Some(x: String) => x
      }
    }
  )


  def main(args: Array[String]): Unit = {

    if (args.length != 1)
      throw new Exception("Not well sent parameters, should only have one, config file")

    //val configJson = new JsonParser("src/test/resource/poConfigTest.json").read.parse
    val configJson = new JsonParser(args(0)).read.parse

    val paths = getCsvPaths(configJson)


    val detalle = configJson.get("dcm_detalle_bac") match {
      case None => throw new Exception("Error reading config file, maybe detalle cabecera doesnt exist")
      case Some(x: Map[String, Object]) => x
      case _ => throw new Exception("Error reading config file, maybe detalle cabecera doesnt exist")
    }

    val cabecera = configJson.get("dcm_cabecera_bac") match {
      case None => throw new Exception("Error reading config file, maybe detalle cabecera doesnt exist")
      case Some(x: Map[String, Object]) => x
      case _ => throw new Exception("Error reading config file, maybe detalle cabecera doesnt exist")
    }

    val detalleGen = new Generator(detalle)
    detalleGen.generarCabecera()
    detalleGen.generarRegistros()
    val detalleResultado = detalleGen.getResultado()

    val cabeceraGen = new Generator(cabecera)
    cabeceraGen.generarCabecera()
    cabeceraGen.generarRegistros()
    val cabeceraResultado = cabeceraGen.getResultado()


    CsvWriter.write(paths.detalleCsvPath, detalleResultado, configJson.getOrElse("delimiter", ";").toString)

    CsvWriter.write(paths.cabeceraCsvPath, cabeceraResultado, configJson.getOrElse("delimiter", ";").toString)


  }

}

package dcm

import java.util.Random

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
//Genera los nombres de los campos de los PO´s ,segun json config
class Generator(val detalle: Map[String, Object]) {

  val numRegistros = detalle.getOrElse("num_registros", "0").asInstanceOf[Int]

  var cabecerasDelResultado: List[String] = _
  var registrosDelResultado: List[List[String]] = _

  val columnas = detalle.get("columna_tipo") match {
    case None => throw new Exception("Error reading config file, maybe detalle cabecera doesnt exist")
    case Some(x: List[Map[String, String]]) => x
  }

   //Generamos los nombres de los campos
  def generarCabecera(): Unit = {
    var nombresDeCampos:List[String] = List()
    var nombre = ""
    for (columna <- columnas) {
      nombre = columna.getOrElse("nombre", "")
      nombresDeCampos = nombresDeCampos ++ List(nombre)

    }
    cabecerasDelResultado = nombresDeCampos
  }

  //Generamos los registros segun el numero segu el conf json llamando al metodo generarRegistro para el random
  def generarRegistros(): Unit = {
    var registros:List[List[String]] = List()
    var resultado: List[String] = List()

    for ( i <- 0 to numRegistros-1) {
      resultado = generarRegistro()
      registros = registros ++ List(resultado)
    }
    registrosDelResultado = registros
  }

  /**
    * generamos un registro con datos aleatorios
    * para ello recorremos las cabeceras, y vamos viendo que timpo tenemos que generar (string, int, etc)
    * dependiendo de el tipo de dato y la longitud generamos uno u otro
    * @return
    */
  def generarRegistro(): List[String] = {
    // TODO
    var resultados: List[String] = List()
    var campo: String = ""
    val randomGenerator = new Random_num()



    for (columna <- columnas) {
      if (columna.getOrElse("nombre", "").toUpperCase == "UN" || columna.getOrElse("nombre", "").toUpperCase == "CAMPANYA" || columna.getOrElse("nombre", "").toUpperCase == "ACCIO" || columna.getOrElse("nombre", "").toUpperCase == "TARGET" || columna.getOrElse("nombre", "").toUpperCase == "SEQACCIO") {
        val campoNombre = columna.getOrElse("nombre", "").toUpperCase match {
          case "UN" => campo = "1235"
          case "CAMPANYA" => campo = "UT478"
          case "ACCIO" => campo = "U567"
          case "TARGET" => campo = "O678"
          case "SEQACCIO" => campo = "45"
        }
      }else if (columna.getOrElse("tipo", "").toUpperCase == "STRING") {
          campo = randomGenerator.str_rand(columna.getOrElse("longitud", "").toInt)
        } else if (columna.getOrElse("tipo", "").toUpperCase == "INT") {
          campo = randomGenerator.num_rand(columna.getOrElse("longitud", "").toInt)
        } else if (columna.getOrElse("tipo", "").toUpperCase == "FLOAT") {
          campo = randomGenerator.flo_rand(columna.getOrElse("longitud", "").toInt, 2)
        } else if (columna.getOrElse("tipo", "").toUpperCase == "DATE") {
          val dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss")
          val random = new Random()
          val datetime = new DateTime(new Random().nextInt()).withMillisOfSecond(0)
          campo = dateTimeFormatter.print(datetime)

        }else {
          throw new Exception("data type not handledd" + columna.getOrElse("tipo", "").toUpperCase)
        }

      resultados = resultados ++ List(campo)
    }

    //resultados = resultados ++ List(campo)


    // iterar lista con los mapas (variable columnas), dichos mapas tienen la longitud y el tipo de dato a devolver
    // for mapa in lista de mapas
    // generar el dato aleatorio en funcion de longitud y tipo como se especifica en el mapa
    // añadir a la lista de resultados

    resultados
  }

    def getCabecera(): List[String] = cabecerasDelResultado

    def getRegistros(): List[List[String]] = registrosDelResultado

    def getResultado(): List[List[String]] = List(cabecerasDelResultado) ++ registrosDelResultado


  }

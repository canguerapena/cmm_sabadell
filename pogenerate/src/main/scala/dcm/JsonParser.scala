package dcm

import scala.io._
import com.lambdaworks.jacks.JacksMapper


class JsonParser (val filePath: String, val fileContent: String = null) {

  def read: JsonParser = new JsonParser(filePath, Source.fromFile(filePath).mkString)

  def parse: Map[String, Object] =
    fileContent match {
      case null => throw new Exception("Not readed file yet")
      case _ => JacksMapper.readValue[Map[String, Object]](fileContent)
    }
}
